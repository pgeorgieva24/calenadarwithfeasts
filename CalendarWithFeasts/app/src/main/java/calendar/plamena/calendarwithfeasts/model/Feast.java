package calendar.plamena.calendarwithfeasts.model;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Feast {
    private int id;
    private int day;
    private int month;
    private int year;
    private boolean isAnnual;
    private String name;

    public Feast(Integer id, int day, int month, int year, boolean isAnnual, String name) {
        this.id = id;
        this.day = day;
        this.month = month;
        this.year = year;
        this.isAnnual = isAnnual;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public boolean isAnnual() {
        return isAnnual;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setAnnual(boolean annual) {
        isAnnual = annual;
    }

    public void setName(String name) {
        this.name = name;
    }

    //    private String formatDate(String dateStr) {
//        try {
//            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
//            Date date = fmt.parse(dateStr);
//            SimpleDateFormat fmtOut = new SimpleDateFormat("MMM d");
//            return fmtOut.format(date);
//        } catch (ParseException e) {
//
//        }
//
//        return "";
//    }
}

