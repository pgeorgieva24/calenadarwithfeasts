package calendar.plamena.calendarwithfeasts;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.Calendar;

import calendar.plamena.calendarwithfeasts.helper.DbManager;
import calendar.plamena.calendarwithfeasts.model.Feast;

public class MainActivity extends AppCompatActivity {

    private DbManager dbManager;
    private CalendarView calendar;
    private TextView feastText;
    private TextView namesText;
    private FloatingActionButton btnAdd;
    private FloatingActionButton btnDelete;
    private int selectedDay;
    private int selectedMonth;
    private int selectedYear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.dbManager = new DbManager(this);
        calendar = findViewById(R.id.calendarView);
        feastText = findViewById(R.id.textViewFeast);
        feastText.setMovementMethod(new ScrollingMovementMethod());
        namesText = findViewById(R.id.textViewNames);
        namesText.setMovementMethod(new ScrollingMovementMethod());
        btnAdd = findViewById(R.id.buttonAdd);
        btnDelete = findViewById(R.id.btnDelete);
        selectedDay = Calendar.getInstance().get(Calendar.DATE);
        selectedMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
        selectedYear = Calendar.getInstance().get(Calendar.YEAR);

        try {
            dbManager.open();
            //dbManager.deleteDb();
        } catch (SQLException E) {
            E.printStackTrace();
            dbManager.close();
        }

        //checkIfDateIsFeast(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DATE));

        Log.e("size of feasts table", String.valueOf(dbManager.getAllFeast().size()));
        Log.e("size of people table", String.valueOf(dbManager.getAllPople().size()));

        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                selectedDay = dayOfMonth;
                selectedMonth = month + 1;
                selectedYear = year;
                checkIfDateIsFeast(year, month + 1, dayOfMonth);
            }
        });



        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), AddFeastActivity.class);
                startActivity(intent);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(v.getContext(), )
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("Delete Feast");
                alert.setMessage("Are you sure you want to delete feast?");
                alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.e("kakvo iskame da iztriem", selectedDay + "/" + selectedMonth + "/" + selectedYear);
                        Feast feast = dbManager.getFeastByDate(selectedDay, selectedMonth, selectedYear);
                        if (feast != null) {
                            dbManager.deleteFeast(feast.getId());
                            checkIfDateIsFeast(selectedYear, selectedMonth, selectedDay);
                        }
                    }
                });
                alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                    }
                });
                alert.show();
            }
        });

    }

    /*@Override
    protected void onStart() {
        super.onStart();
        Log.e("from on start", "hello");
        checkIfDateIsFeast(selectedYear, selectedMonth, selectedDay);
    }
*/
    @Override
    protected void onResume() {
        super.onResume();
        Log.e("from on resume", "hello on resume");

        checkIfDateIsFeast(selectedYear, selectedMonth, selectedDay);

    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.e("from on pause", "hello on pause let this work afer i delete item");

        checkIfDateIsFeast(selectedYear, selectedMonth, selectedDay);
    }

    public void checkIfDateIsFeast(int year, int month, int dayOfMonth) {
        Feast feast = dbManager.getFeastByDate(dayOfMonth, month, year);
        if (feast != null) {
            feastText.setText(feast.getName());
            String names = dbManager.getNamesForFeast(feast.getId());
            if (names != null) namesText.setText(names);
            else namesText.setText("");
        } else {
            feastText.setText(getResources().getString(R.string.textVeiwFeasts));
            namesText.setText("");
        }
    }

}
