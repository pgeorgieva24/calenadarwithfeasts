package calendar.plamena.calendarwithfeasts;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import calendar.plamena.calendarwithfeasts.helper.DbManager;
import calendar.plamena.calendarwithfeasts.model.Feast;
import calendar.plamena.calendarwithfeasts.model.Person;

public class AddFeastActivity extends AppCompatActivity {

    private EditText etDate;
    private CheckBox isAnnual;
    private EditText feastName;
    private Button btnCancel;
    private Button btnOk;
    private Button btnNames;
    private DbManager dbManager;
    ArrayList<Integer> mSelectedItems;
    List<Integer> selectedPeopleIds;
    private TextView tVNames;

    private DatePickerDialog.OnDateSetListener mDateSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_feast);

        etDate = findViewById(R.id.etDate);
        isAnnual = findViewById(R.id.cbIsAnnual);
        feastName = findViewById(R.id.etFeastName);
        btnCancel = findViewById(R.id.btnCancel);
        btnOk = findViewById(R.id.btnOk);
        btnNames = findViewById(R.id.btnNames);
        selectedPeopleIds = new ArrayList<>();
        tVNames = findViewById(R.id.tVNames);
        dbManager = new DbManager(this);

        try {
            dbManager.open();
        } catch (SQLException E) {
            E.printStackTrace();
            dbManager.close();
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etDate.getText().length() == 0 && feastName.getText().length() == 0) {
                    Toast.makeText(AddFeastActivity.this, "Please fill date field and feast name", Toast.LENGTH_LONG).show();
                } else if (feastName.getText().length() == 0) {
                    Toast.makeText(AddFeastActivity.this, "Please fill feast name", Toast.LENGTH_LONG).show();
                } else if (etDate.getText().length() == 0) {
                    Toast.makeText(AddFeastActivity.this, "Please fill date field ", Toast.LENGTH_LONG).show();
                } else {
                    String date = etDate.getText().toString();
                    int day = Integer.valueOf(date.substring(0, 2));
                    int month = Integer.valueOf(date.substring(3, date.lastIndexOf('/')));
                    int year = Integer.valueOf(date.substring(date.lastIndexOf('/') + 1));

                    Feast feast = new Feast(-1, day, month, year, isAnnual.isSelected(), feastName.getText().toString());

                    try {
                        dbManager.createFeast(feast);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (selectedPeopleIds != null) {
                        int feastId = dbManager.getFeastByDate(day, month, year).getId();
                        for (int id : selectedPeopleIds) {
                            try {
                                dbManager.createFeastsPeople(feastId, id);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    finish();
                }
            }
        });


        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(AddFeastActivity.this,
                        R.style.DatePickerTheme, mDateSetListener, year, month, day);

                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                etDate.setText(String.valueOf(dayOfMonth) + "/" + String.valueOf(month + 1) + "/" + String.valueOf(year));
            }
        };

        btnNames.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // where we will store or remove selected items
                tVNames.setText("");
                selectedPeopleIds.clear();
                mSelectedItems = new ArrayList<>();
                final List<Person> people = dbManager.getAllPople();
                final String[] peopleNames = new String[people.size()];
                if (people != null) {
                    for (int i = 0; i < people.size(); i++) {
                        peopleNames[i] = people.get(i).getName();
                    }

                    AlertDialog.Builder builder = new AlertDialog.Builder(AddFeastActivity.this);

                    builder.setMultiChoiceItems(peopleNames, null, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which, boolean isChecked) {

                            if (isChecked)
                                mSelectedItems.add(which);
                            else if (mSelectedItems.contains(which))
                                mSelectedItems.remove(which);
                        }
                    });
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String names = "";
                            if (mSelectedItems.size() != 0) {
                                for (int itemPosition : mSelectedItems) {
                                    String name = peopleNames[itemPosition];
                                    names += name + ", ";
                                    for (Person p : people) {
                                        if (p.getName() == name) {
                                            selectedPeopleIds.add(p.getId());
                                            break;
                                        }
                                    }
                                }
                                tVNames.setText(names.substring(0, names.lastIndexOf(',')));
                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    builder.show();
                }

            }
        });

    }

}
