package calendar.plamena.calendarwithfeasts.helper;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import calendar.plamena.calendarwithfeasts.model.Feast;
import calendar.plamena.calendarwithfeasts.model.Person;


public class DbManager {

    private DatabaseHelper dbHelper;
    private Context context;
    private SQLiteDatabase db;

    public DbManager(Context c) {
        context = c;
    }

    public DbManager open() throws SQLException {
        dbHelper = new DatabaseHelper(context);

        db = dbHelper.getWritableDatabase();
        return this;
    }


    public void close() {
        dbHelper.closeDB();
    }
/*
    public void deleteDb() {
        context.deleteDatabase("calendarWithFeasts");
    }*/

    public void createFeast(Feast feasts) {

        ContentValues values = new ContentValues();
        values.put(dbHelper.KEY_DAY, feasts.getDay());
        values.put(dbHelper.KEY_MONTH, feasts.getMonth());
        values.put(dbHelper.KEY_YEAR, feasts.getYear());
        values.put(dbHelper.KEY_IS_ANNUAL, feasts.isAnnual());
        values.put(dbHelper.KEY_FEAST_NAME, feasts.getName());

        db.insert(dbHelper.TABLE_FEASTS, null, values);
    }

    public Feast getFeastById(long feastId) {
        String selectQuery = "SELECT * FROM " + dbHelper.TABLE_FEASTS + " WHERE " + dbHelper.KEY_ID + " = " + feastId;

        Log.e(dbHelper.LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            c.moveToFirst();
        }

        Feast feast = new Feast(c.getInt(c.getColumnIndex(dbHelper.KEY_ID)), c.getInt(c.getColumnIndex(dbHelper.KEY_DAY)),
                c.getInt(c.getColumnIndex(dbHelper.KEY_MONTH)), c.getInt(c.getColumnIndex(dbHelper.KEY_YEAR)),
                Boolean.parseBoolean(c.getString(c.getColumnIndex(dbHelper.KEY_IS_ANNUAL))),
                c.getString(c.getColumnIndex(dbHelper.KEY_FEAST_NAME)));

        return feast;
    }

    public List<Feast> getAllFeast() {

        List<Feast> feastList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + dbHelper.TABLE_FEASTS;

        Log.e(dbHelper.LOG, selectQuery);


        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                Feast feast = new Feast(c.getInt(c.getColumnIndex(dbHelper.KEY_ID)),
                        c.getInt(c.getColumnIndexOrThrow(dbHelper.KEY_DAY)),
                        c.getInt(c.getColumnIndexOrThrow(dbHelper.KEY_MONTH)),
                        c.getInt(c.getColumnIndexOrThrow(dbHelper.KEY_YEAR)),
                        Boolean.parseBoolean(c.getString(c.getColumnIndexOrThrow(dbHelper.KEY_IS_ANNUAL))),
                        c.getString(c.getColumnIndexOrThrow(dbHelper.KEY_FEAST_NAME)));
                feastList.add(feast);
            } while (c.moveToNext());
        }


        return feastList;
    }

    public int updateFeast(Feast feast) {
        ContentValues values = new ContentValues();
        values.put(dbHelper.KEY_DAY, feast.getDay());
        values.put(dbHelper.KEY_MONTH, feast.getMonth());
        values.put(dbHelper.KEY_YEAR, feast.getYear());
        values.put(dbHelper.KEY_IS_ANNUAL, feast.isAnnual());
        values.put(dbHelper.KEY_FEAST_NAME, feast.getName());

        return db.update(dbHelper.TABLE_FEASTS, values, dbHelper.KEY_ID + " = ?", new String[]{String.valueOf(feast.getId())});
    }

    public void deleteFeast(long feastId) {
        Log.e(dbHelper.LOG, feastId+" is to be deleted");

        db.delete(dbHelper.TABLE_FEASTS, dbHelper.KEY_ID + " = ?", new String[]{String.valueOf(feastId)});
    }

    //endregion
    //region People
    public long createPerson(Person people) {
        ContentValues values = new ContentValues();

        values.put(dbHelper.KEY_PERSON_NAME, people.getName());

        long personId = db.insert(dbHelper.TABLE_PEOPLE, null, values);
        return personId;
    }

    public Person getPersonById(long personId) {
        String selectQuery = "SELECT * FROM " + dbHelper.TABLE_PEOPLE + " WHERE " + dbHelper.KEY_ID + " = " + personId;

        Log.e(dbHelper.LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            c.moveToFirst();
        }

        Person person = new Person(c.getInt(c.getColumnIndex(dbHelper.KEY_ID)), c.getString(c.getColumnIndex(dbHelper.KEY_PERSON_NAME)));

        return person;
    }

    public List<Person> getAllPople() {
        List<Person> personList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + dbHelper.TABLE_PEOPLE;

        Log.e(dbHelper.LOG, selectQuery);


        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                Person person = new Person(c.getInt(c.getColumnIndex(dbHelper.KEY_ID)), c.getString(c.getColumnIndex(dbHelper.KEY_PERSON_NAME)));
                personList.add(person);
            } while (c.moveToNext());
        }

        return personList;
    }

    public int updatePerson(Person person) {
        ContentValues values = new ContentValues();
        values.put(dbHelper.KEY_PERSON_NAME, person.getName());

        return db.update(dbHelper.TABLE_PEOPLE, values, dbHelper.KEY_ID + " = ?", new String[]{String.valueOf(person.getId())});
    }

    public void deletePerson(long personId) {
        db.delete(dbHelper.TABLE_PEOPLE, dbHelper.KEY_ID + " = ?", new String[]{String.valueOf(personId)});
    }

    //endregion
    //region FeastsPeople
    public long createFeastsPeople(long feastId, long personId) {
        ContentValues values = new ContentValues();

        values.put(dbHelper.KEY_FEAST_ID, feastId);
        values.put(dbHelper.KEY_PERSON_ID, personId);

        long feastPeopleId = db.insert(dbHelper.TABLE_FEASTS_PEOPLE, null, values);
        return feastPeopleId;
    }

    public int updatePersonIdInFeastsPeople(long id, long personId) {
        ContentValues values = new ContentValues();
        values.put(dbHelper.KEY_PERSON_ID, personId);

        return db.update(dbHelper.TABLE_FEASTS_PEOPLE, values, dbHelper.KEY_ID + " = ?", new String[]{String.valueOf(personId)});
    }

    // dont think i need it dont know yet
    /*public int updateFeastsPeople(long feastId, long personId) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_FEAST_ID, feastId);
        values.put(KEY_PERSON_ID, personId);

        return db.update(TABLE_FEASTS_PEOPLE, values, KEY_ID + " = ?", new String[]{String.valueOf(its own id});
    }*/

    public void deleteFeastsPeople(long feastsPeopleId) {
        db.delete(dbHelper.TABLE_PEOPLE, dbHelper.KEY_ID + " = ?", new String[]{String.valueOf(feastsPeopleId)});
    }
    //endregion


    public Feast getFeastByDate(int day, int month, int year) {
        String selectQuery = "SELECT *" + " FROM " + dbHelper.TABLE_FEASTS + " WHERE " + dbHelper.KEY_DAY + " = " + day + " AND "
                + dbHelper.KEY_MONTH + " = " + month + " AND " + dbHelper.KEY_YEAR + " = " + year;

        Log.e(dbHelper.LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        Feast feast = null;
        if (c.moveToFirst()) {
            feast = new Feast(c.getInt(c.getColumnIndex(dbHelper.KEY_ID)),
                    c.getInt(c.getColumnIndex(dbHelper.KEY_DAY)),
                    c.getInt(c.getColumnIndex(dbHelper.KEY_MONTH)),
                    c.getInt(c.getColumnIndex(dbHelper.KEY_YEAR)),
                    Boolean.parseBoolean(c.getString(c.getColumnIndex(dbHelper.KEY_IS_ANNUAL))),
                    c.getString(c.getColumnIndex(dbHelper.KEY_FEAST_NAME)));
        }
        return feast;
    }

    public List<Integer> getPeopleIdsFromFeastsPeople(int feastId) {
        List<Integer> peopleIds = new ArrayList<>();

        String selectQuery = "SELECT " + dbHelper.KEY_PERSON_ID + " FROM " + dbHelper.TABLE_FEASTS_PEOPLE + " WHERE "
                + dbHelper.KEY_FEAST_ID + " = " + feastId;

        Log.e(dbHelper.LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);

        if (c.moveToFirst()) {
            do {
                int personId = c.getInt(c.getColumnIndex(dbHelper.KEY_PERSON_ID));
                peopleIds.add(personId);
            } while (c.moveToNext());
        }
        return peopleIds;
    }

    public String getPersonName(long personId) {
        String selectQuery = "SELECT " + dbHelper.KEY_PERSON_NAME + " FROM " + dbHelper.TABLE_PEOPLE + " WHERE " + dbHelper.KEY_ID + " = " + personId;

        Log.e(dbHelper.LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null) {
            c.moveToFirst();
        }

        String name = c.getString(c.getColumnIndex(dbHelper.KEY_PERSON_NAME));

        return name;
    }

    public String getNamesForFeast(int feastId) {
        String names = "\n";

        List<Integer> peopleIds = getPeopleIdsFromFeastsPeople(feastId);

        if (peopleIds.size() > 0) {
            for (int personId : peopleIds) {
                names += (getPersonName(personId) + ", ");
            }
            names = names.substring(0, names.lastIndexOf(','));
        }
        return names;
    }
}